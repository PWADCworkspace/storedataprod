const fspath = require("path");
var ss = pjs.require("InHouseApps" + fspath.sep +  "storedataprod" + fspath.sep +  "splitState.js");
var dp = pjs.require("InHouseApps" + fspath.sep +  "storedataprod" + fspath.sep +  "detectpo.js");

function refreshstores(){

    var sqlquery = "delete from profound.storesp where datasource = 'AP' WITH NONE";
    var addr3;
    var regex;
    var regex2;
    var storeNumber;
    var zip;
    pjs.query(sqlquery);
    //console.log("delete sqlcode=" +  sqlcode);

    sqlquery =  "select #vndno, #name, #addr1, #addr2, #addr3, #acctn " + 
                "from pwafil.APFILE where #vndno > 9000 and #vndno <=9999";

    var apStores = pjs.query(sqlquery)
    if (apStores && apStores.length >0)
    {
        for (var i=0 ; i < apStores.length ; i++)
        {
            addr3 =  apStores[i]["#addr3"];
            storeNumber = apStores[i]["#vndno"] % 9000;
            if (storeNumber > 0)
            {
                regex =addr3.match(/[0-9]{5}/);  //zip format 12345
                regex2 =addr3.match(/^[0-9]{5}-[0-9]{4}/); //zip format 12345-6789
                zip = "";
                if (regex2)
                    zip = regex2[0];

                if (!regex2 && regex)
                    zip = regex[0];

                insertRecord(storeNumber, apStores[i], zip);
            }
            //regex = addr3.match(/\s+[0-9]{1,3}$/);
            //if (regex)
            //{
                //storeNumber = regex[0].trim();
                //if (storeNumber.length < 4)
                //{
                //}
            //}
        }
    }




    function insertRecord(storeNumber, apStore, zip){
        var ssObj = ss.splitState(apStore["#addr2"]);
        var dpObj = dp.detectpo(apStore["#addr1"]);
        var dataSource = 'AP';
        var SqlqueryInsert = "insert into storesp " + 
        "(StoreNum, Datasource, Marquename, StrStreet, StrCity, StrZip, State, POBox, UpdatedUsr,  UpdatedTmz)  " +
        "values(?,?,?,?,?,?,?,?,?,CURRENT_TIMESTAMP) with NONE";
        pjs.query(SqlqueryInsert, [ storeNumber,
                                    dataSource, 
                                    apStore["#name"],
                                    dpObj["address"],
                                    ssObj["city"],
                                    zip,
                                    ssObj["state"],
                                    dpObj["pobox"],
                                    pjs.getUser().toUpperCase()
                                    ]);
                                    //console.log("ap sqlcode="+ sqlcode);
                                    if (sqlcode !=0)
                                    {
                                        console.log(apStore);
                                        console.log(JSON.stringify(ssObj));
                                        console.log(JSON.stringify(dpObj));
                                        
                                    }
                                   
    
    }


}
exports.refreshstores = refreshstores;