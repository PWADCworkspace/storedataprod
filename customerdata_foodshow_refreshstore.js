function refreshstore(storeId) {

    //process.env["PJS_SQL_DEBUG"] = "1"; //used for debugging sql errors

    clientId = "0221601c-1119-4c76-81b5-7bcc0a83d043";

    dataSource = 'FOODSHOW';

    //console.info(`Attempting to refresh ${dataSource} data source record for store ${storeId}`);

    try {

        var token = pjs.sendRequest({
            method: "get",
            uri: "https://gopwadc.com/api/token?clientId=" + clientId
        });
    
        var store = pjs.sendRequest({
            headers: {
                'Authorization' : 'Bearer ' + token
            },
            method: "get",
            uri: "https://gopwadc.com/api/stores/" + storeId
        });


        //delete existing foodshow store entry in db
        pjs.query(`DELETE FROM profound.storesp WHERE DATASOURCE = '${dataSource}' AND StoreNum = '${storeId}' with NONE`); 
        //pjs.query("DELETE FROM storesp WHERE DATASOURCE = ? AND StoreNum = ? with NONE", [dataSource, StoreNum]);   
    
        var query =  "insert into profound.storesp " + 
        "(StoreNum, MarqueName, phoneNum, StrStreet, StrCity, State, StrZip, Owner, Manager, FaxNum, DataSource, UpdatedUsr, UpdatedTmz)  " +
        "values(?,?,?,?,?,?,?,?,?,?,?,?,CURRENT_TIMESTAMP) with NONE";
        //console.log(query);

        pjs.query(query, [
            store.Id,
            (store.Name == null) ? "" : store.Name,
            (store.Phone == null) ? "" : store.Phone,
            (store.Address == null) ? "" : store.Address,
            (store.City == null) ? "" : store.City,
            (store.State == null) ? "" : store.State,
            (store.ZipCode == null) ? "" : store.ZipCode,
            (store.Owner == null) ? "" : store.Owner,
            (store.Manager == null) ? "" : store.Manager,
            (store.Fax == null) ? "" : store.Fax,
            dataSource,
            pjs.getUser().toUpperCase()
            ]);

            if (sqlcode !=0)
            console.error('An error occurred while attempting to insert store ' + store.Id + '  sqlcode: ' + sqlcode);
    }
    catch(exception) {
        console.error(`An error occured while attempting to refresh ${dataSource} data source record for store ${storeId}: ${exception}`);
    }    
}

exports.refreshstore = refreshstore;