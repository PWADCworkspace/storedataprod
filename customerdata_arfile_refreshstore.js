const fspath = require("path");
var ss = pjs.require("InHouseApps" + fspath.sep +  "storedataprod" + fspath.sep +  "splitState.js")

function refreshstore(storenumber){

    var sqlquery = "delete from storesp where datasource = 'AR' " + 
                    "AND StoreNum = ? WITH NONE";
    pjs.query(sqlquery,[storenumber]);
    

    var sqlqueryIn =  "select ARFKEY, STNAME, STADRS, STPO, STCITY, STZIP, STATUS " + 
                    "from pwafil.ARFILE " +
                    "WHERE status <> 'D' " + 
                    "AND stname NOT LIKE '%DUMMY%' " +
                    "AND STADRS NOT LIKE '%DUMMY%' " + 
                    "AND ARFKEY = ?";
  
    var arStores = pjs.query(sqlqueryIn,[storenumber]);
    if (arStores && arStores.length >0)
    {
       
             insertRecord(arStores[0]);
       
    }



function insertRecord(arStore){
    var ssObj = ss.splitState(arStore["stcity"]);
    var pobox = arStore["stpo"];
    if (isNaN(pobox))
    {
        pobox = "";
    }
    else
    {
        pobox = Number(pobox);
    }
    if (pobox==0)  pobox = "";
    if (pobox != "") pobox = "P.O. BOX " + String(pobox).trim();
    
    //console.log(ssObj["city"] + "       (" + ssObj["state"] +")");
    var dataSource = 'AR';
    var SqlqueryInsert = "insert into profound.storesp " + 
    "(StoreNum, Datasource, Marquename, StrStreet, StrCity, StrZip, State, status, POBox, UpdatedUsr,  UpdatedTmz)  " +
    "values(?,?,?,?,?,?,?,?,?,?,CURRENT_TIMESTAMP) with NONE";
    pjs.query(SqlqueryInsert, [ arStore["arfkey"], 
                                dataSource, 
                                arStore["stname"],
                                arStore["stadrs"],
                                ssObj["city"],
                                arStore["stzip"],
                                ssObj["state"],
                                arStore["status"],
                                pobox,
                                pjs.getUser().toUpperCase()
                                ]);
                                

}


}

exports.refreshstore = refreshstore;