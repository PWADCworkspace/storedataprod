const fspath = require("path");
var ss = pjs.require("InHouseApps" + fspath.sep +  "storedataprod" + fspath.sep +  "splitState.js");

function refreshstores(){


    var sqlquery = "delete from profound.storesp where datasource = 'STOREFIL' WITH NONE";
    pjs.query(sqlquery);

    sqlquery =  "select store#, phone#, street, cityst, zipcd, owner, managr, faxnum " + 
                "from pwafil.STOREFIL";

    var storefils = pjs.query(sqlquery)
        if (storefils && storefils.length >0)
        {
            for (var i=0 ; i < storefils.length ; i++)
            {
                insertRecord(storefils[i]);
            }
        }





        function insertRecord(storefil){
            var ssObj = ss.splitState(storefil["cityst"])
            var dataSource = 'STOREFIL';
            var SqlqueryInsert = "insert into profound.storesp " + 
            "(StoreNum, Datasource, phoneNum, StrStreet, StrCity, StrZip, State, Owner, Manager, FaxNum, UpdatedUsr,  UpdatedTmz)  " +
            "values(?,?,?,?,?,?,?,?,?,?,?,CURRENT_TIMESTAMP) with NONE";
            pjs.query(SqlqueryInsert, [ storefil["store#"], 
                                        dataSource, 
                                        storefil["phone#"],
                                        storefil["street"],
                                        ssObj["city"],
                                        storefil["zipcd"],
                                        ssObj["state"],
                                        storefil["owner"],
                                        storefil["managr"],
                                        storefil["faxnum"],
                                        pjs.getUser().toUpperCase()
                                        ]);
        }

}





exports.refreshstores = refreshstores;