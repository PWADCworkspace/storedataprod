/*======================= Create STORESP Table ==========================*/
create table profound.storesp(
StoreNum DECIMAL(3, 0) NOT NULL DEFAULT 0 ,
DataSource char(10) NOT NULL DEFAULT '',
Status char(1) NOT NULL DEFAULT '',
Pig char(1) NOT NULL DEFAULT '',
storeOpen char(1) NOT NULL DEFAULT '',
MarqueName char(35) NOT NULL DEFAULT '',
StrStreet char(35) NOT NULL DEFAULT '',
StrCity char(35) NOT NULL DEFAULT '',
StrZip char(20) NOT NULL DEFAULT '',
State char(2) NOT NULL DEFAULT '',
ShipStreet char(35) NOT NULL DEFAULT '',
ShipCity char(35) NOT NULL DEFAULT '',
ShipZip char(20) NOT NULL DEFAULT '',
phoneNum char(20) NOT NULL DEFAULT '',
FaxNum char(20) NOT NULL DEFAULT '',
Owner char(50) NOT NULL DEFAULT '',
Manager char(50) NOT NULL DEFAULT '',
storeemail char(50) NOT NULL DEFAULT '',
owneremail char(50) NOT NULL DEFAULT '',
PRIMARY KEY( StoreNum, DataSource ) )
RCDFMT storesr;

LABEL ON TABLE profound.storesp
	IS 'Store Repository' ;

LABEL ON COLUMN profound.storesp(
    StoreNum TEXT IS 'Store Number' ,
    DataSource TEXT IS 'Data Source',
    Status TEXT IS 'Status',
	Pig TEXT IS 'Pig Y/N' ,
	storeOpen TEXT IS 'Store Open Y/N' ,
	MarqueName TEXT IS 'Store Marquee Name' ,
	StrStreet TEXT IS 'Store Street' ,
	StrCity TEXT IS 'Store City',
    StrZip TEXT IS 'Store Zip',
    State TEXT IS 'State',
    ShipStreet TEXT IS 'Stip Street',
    ShipCity TEXT IS 'Store City',
    ShipZip TEXT IS 'Ship Zip',
    phoneNum TEXT IS 'Phone Number',
    FaxNum TEXT IS 'Fax Number',
    Owner TEXT IS 'Owner',
    Manager TEXT IS 'Manager',
    storeemail TEXT IS 'Store Email',
    owneremail TEXT IS 'Owner Email'
    ) ;
/* ==============================================================*/    