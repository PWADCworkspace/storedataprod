// Load store List.xlsx to the storesp database table

function loadexcel (xlsxname, dirname, parmStatus) {

    pjs.define("xlsxname", { type: 'char', length: 256, refParm: xlsxname });
    pjs.define("dirname", { type: 'char', length: 256, refParm: dirname });
    pjs.define("parmStatus", {
        type: 'ds', qualified: true, elements: {
            "statement"  : { type: "char", length:  2 },
            "state"      : { type: "char", length:  5 }
        }, refParm: parmStatus
    });
    var directory = dirname;
    var fileName = directory.trim() + '/' + xlsxname;
    fileName = fileName.trim();
    
    var xlsx = require ("xlsx");
    
    var wb;
    try{
        wb = xlsx.readFile (fileName, {cellDates: true});  //####### do we need cellDates here??
    }
    catch(err){
        parmStatus.statement = "10";
        logError(parmStatus);
        return;
    }
    
    var ws;
    try{ws = wb.Sheets ["August 18, 2020"];
    }
    catch(err){
        parmStatus.statement = "20";
        logError(parmStatus);
        return;   
    }
    
    
    var sqlquery = "delete from storesp where datasource = 'EX' WITH NONE";
    pjs.query(sqlquery);
    console.log("delete sqlcode=" +  sqlcode);


    var data = xlsx.utils.sheet_to_json (ws);
    sqlquery =  "insert into storesp " + 
                "(Pig, StoreNum, DataSource, phoneNum, StrStreet, StrCity, State, StrZip, Owner, Manager, FaxNum, UpdatedUsr)  " +
                "values(?,?,?,?,?,?,?,?,?,?,?,?) with NONE";


    for (i=0 ; i < data.length ; i++)  
    {
         
        pjs.query(sqlquery, [
            data[i]["Pig"],
            data[i]["No."],
            "EX",
            data[i]["Phone"],
            data[i]["Street"],
            data[i]["City"],
            data[i]["St."].replace(".",""),
            data[i]["Zip"],
            data[i]["Owner"],
            data[i]["Manager"],
            data[i]["Fax"],
            "PROFOUND"
            ]);
        if (sqlstate >= "02000")
        {
            parmStatus.statement = "30";
            parmStatus.state = sqlstate;
            logError(parmStatus);
            return;
        }



    }    
    //pjs.query("commit");
    
    function logError(parmStatus){
        var pgm = "loadexcel";
        var tz = pjs.timestamp();
        var message = tz + " " + pgm +    ":  statement=" + parmStatus.statement + ",  state=" + parmStatus.state;
        console.log(message);
        console.log("sqlcod: " + sqlcod);
        }
        

}

exports.run = loadexcel;
exports.parms = [
    { type: 'char', length: 256 },
    { type: 'char', length: 256 },
    {
        type: 'ds', qualified: true, elements: {
        "statement"  : { type: "char", length:  2 },
        "state"      : { type: "char", length:  5 }
        }
    }    
];