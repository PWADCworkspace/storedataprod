const fspath = require("path");
var ss = pjs.require("InHouseApps" + fspath.sep +  "storedataprod" + fspath.sep +  "splitState.js");
var dp = pjs.require("InHouseApps" + fspath.sep +  "storedataprod" + fspath.sep +  "detectpo.js");

function refreshstore(storeNumber){
    
    var sqlquery = "delete from storesp where datasource = 'AP' " + 
                    "and StoreNum = ? WITH NONE";
    var addr3;
    var regex;
    var regex2;
    var vendorNumber = Number(9000) + Number(storeNumber);
    
    var zip;
    pjs.query(sqlquery,[storeNumber]);
    

    sqlquery =  "select #vndno, #name, #addr1, #addr2, #addr3, #acctn " + 
                "from pwafil.APFILE where #vndno = ? ";
               

    var apStores = pjs.query(sqlquery,[ vendorNumber ])
    if (apStores && apStores.length >0)
    {
       
            addr3 =  apStores[0]["#addr3"];
            if (storeNumber > 0)
            {
                regex =addr3.match(/[0-9]{5}/);  //zip format 12345
                regex2 =addr3.match(/^[0-9]{5}-[0-9]{4}/); //zip format 12345-6789
                zip = "";
                if (regex2)
                    zip = regex2[0];

                if (!regex2 && regex)
                    zip = regex[0];

                insertRecord(storeNumber, apStores[0], zip);
            }
        
    }




    function insertRecord(storeNumber, apStore, zip){
        var ssObj = ss.splitState(apStore["#addr2"])
        var dpObj = dp.detectpo(apStore["#addr1"]);
        var dataSource = 'AP';
        var SqlqueryInsert = "insert into storesp " + 
        "(StoreNum, Datasource, Marquename, StrStreet, StrCity, StrZip, State, POBox, UpdatedUsr,  UpdatedTmz)  " +
        "values(?,?,?,?,?,?,?,?,?,CURRENT_TIMESTAMP) with NONE";
        pjs.query(SqlqueryInsert, [ storeNumber,
                                    dataSource, 
                                    apStore["#name"],
                                    dpObj["address"],
                                    ssObj["city"],
                                    zip,
                                    ssObj["state"],
                                    dpObj["pobox"],
                                    pjs.getUser().toUpperCase()
                                    ]);
    }


}
exports.refreshstore = refreshstore;