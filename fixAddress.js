 function fixAddressx(address){
    
    address = address.trim();
    var addLen = address.length;
    var last2;
    var last3;

    //remove trailing . unless N. S. E. W.
    if (addLen > 3 && address.substr(addLen-1, 1) == ".")
    {
        last3 = address.substr(addLen-3, 3);
        if (last3 != " N."  && last3 != " S."  && last3 != " E."  && last3 != " W." )
            address = address.substr(0, addLen-1 );
    }

    // Add trailing . to N S E W
    address = address.trim();
    addLen = address.length;
    if (addLen > 3 && address.substr(addLen-1, 1) != ".")
    {
        last2 = address.substr(addLen-2, 2);
        if (last2 == " N"  ||  last2 == " S" || last2 == " E"  || last2 == " W"  )
            address = address + ".";
    }

    address = address + " ";
    address = address.replace(/  +/g, ' ');
    address = address.replace(" NORTH ", " N. ");
    address = address.replace(" SOUTH ", " S. ");
    address = address.replace(" EAST ", " E. ");
    address = address.replace(" WEST ", " W. ");
    address = address.replace(" STREET ", " ST ");
    address = address.replace(" DRIVE ", " DR ");
    address = address.replace(" ROAD ", " RD ");
    address = address.replace(" HIGHWAY ", " HWY ");
    address = address.replace(" AVENUE ", " AVE ");
    address = address.trim();
    

    return address;
}

 exports.fixAddressx = fixAddressx;