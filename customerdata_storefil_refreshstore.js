const fspath = require("path");
var ss = pjs.require("InHouseApps" + fspath.sep +  "storedataprod" + fspath.sep +  "splitState.js");

function refreshstore(storeNumber){

    var sqlquery = "delete from profound.storesp where datasource = 'STOREFIL' " + 
    "and StoreNum = ? WITH NONE";
    pjs.query(sqlquery,[storeNumber]);

    sqlquery =  "select store#, phone#, street, cityst, zipcd, owner, managr, faxnum " + 
                "from pwafil.STOREFIL where store# = ? ";

    var storefils = pjs.query(sqlquery,[storeNumber]);
        if (storefils && storefils.length >0)
        {
                insertRecord(storefils[0]);
        }





        function insertRecord(storefil){
            var ssObj = ss.splitState(storefil["cityst"])
            var dataSource = 'STOREFIL';
            var SqlqueryInsert = "insert into profound.storesp " + 
            "(StoreNum, Datasource, phoneNum, StrStreet, StrCity, StrZip, State, Owner, Manager, FaxNum, UpdatedUsr,  UpdatedTmz)  " +
            "values(?,?,?,?,?,?,?,?,?,?,?,CURRENT_TIMESTAMP) with NONE";
            pjs.query(SqlqueryInsert, [ storefil["store#"], 
                                        dataSource, 
                                        storefil["phone#"],
                                        storefil["street"],
                                        ssObj["city"],
                                        storefil["zipcd"],
                                        ssObj["state"],
                                        storefil["owner"],
                                        storefil["managr"],
                                        storefil["faxnum"],
                                        pjs.getUser().toUpperCase()
                                        ]);
        }
}





exports.refreshstore = refreshstore;