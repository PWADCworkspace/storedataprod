function refreshstores() {
    
    clientId = "0221601c-1119-4c76-81b5-7bcc0a83d043";

    dataSource = 'FOODSHOW';

    var token = pjs.sendRequest({
        method: "get",
        uri: "https://gopwadc.com/api/token?clientId=" + clientId
    });

    var stores = pjs.sendRequest({
        headers: {
            'Authorization' : 'Bearer ' + token
        },
        method: "get",
        uri: "https://gopwadc.com/api/stores"
    });

    //pjs.query(`DELETE FROM storesp WHERE DATASOURCE = '${dataSource}' with NONE`);
    pjs.query("DELETE FROM profound.storesp WHERE DATASOURCE = ? with NONE",[dataSource]);

    var sqlquery =  "insert into profound.storesp " + 
                    "(StoreNum, MarqueName, phoneNum, StrStreet, StrCity, State, StrZip, Owner, Manager, FaxNum, DataSource, UpdatedUsr, UpdatedTmz)  " +
                    "values(?,?,?,?,?,?,?,?,?,?,?,?,CURRENT_TIMESTAMP) with NONE";

    for (var i in stores) {

        var store = stores[i];

        try {

            pjs.query(sqlquery, [
                store.Id,
                (store.Name == null) ? "" : store.Name,
                (store.Phone == null) ? "" : store.Phone,
                (store.Address == null) ? "" : store.Address,
                (store.City == null) ? "" : store.City,
                (store.State == null) ? "" : store.State,
                (store.ZipCode == null) ? "" : store.ZipCode,
                (store.Owner == null) ? "" : store.Owner,
                (store.Manager == null) ? "" : store.Manager,
                (store.Fax == null) ? "" : store.Fax,
                dataSource,
                pjs.getUser().toUpperCase()
                ]);
        }
        catch(exception) {
            console.error('An error occured while attempting to insert store ' + store.Id + ': ' + exception);
        }
    }

  
}

exports.refreshstores = refreshstores;