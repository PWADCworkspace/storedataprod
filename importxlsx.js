function importxlsx(xlsxname, dirname, parmStatus) {
  pjs.define('xlsxname', { type: 'char', length: 256, refParm: xlsxname });
  pjs.define('dirname', { type: 'char', length: 256, refParm: dirname });
  pjs.define('parmStatus', {
    type: 'ds',
    qualified: true,
    elements: {
      statement: { type: 'char', length: 2 },
      state: { type: 'char', length: 7 },
      message: { type: 'char', length: 100 },
    },
    refParm: parmStatus,
  });

  var directory = dirname;
  var fileName = directory.trim() + '/' + xlsxname;
  fileName = fileName.trim();

  var xlsx = require('xlsx');
  console.log('fileName:' + fileName);

  try {
    wb = xlsx.readFile(fileName, { cellDates: true });
  } catch (err) {
    parmStatus.statement = '10';
    parmStatus.message = 'Error Opening ' + fileName;
    logError(parmStatus);
    return;
  }

  var ws;
  try {
    ws = wb.Sheets['Sheet1'];
  } catch (err) {
    parmStatus.statement = '20';
    logError(parmStatus);
    return;
  }

  var data = xlsx.utils.sheet_to_json(ws);
  var sqlquery =
    'insert into ADVITMSS ' + 
    'VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) with NONE';

  var adYear;
  var adNumber;
  var itemCode;
  var itemDesc;
  var multi;
  var adSrp;
  var curCost;
  var feature;
  var lumpSum;
  var coopRebateCase;
  var retailRebateCase;
  var promoStart;
  var promoStart3d; //promoStart + 3 days in date format
  var promoStart3n; //promoStart + 3 days in number format
  var promoEnd;
  var promoEndd;
  var holdOutStart;
  var holdOutEnd;
  var comments;
  var tempRebStart;
  var tempRebEnd;
  var tempAdDate;

  for (i = 0; i < data.length; i++) {
    adYear = 0;
    adNumber = 0;
    itemCode = 0;
    itemDesc = '';
    multi = 0;
    adSrp = 0;
    curCost = 0;
    feature = 0;
    lumpSum = 0;
    coopRebateCase = 0;
    retailRebateCase = 0;
    promoStart = 0;
    promoStart3d = '';
    promoStart3n = 0;
    promoEnd = 0;
    promoEndd = '';
    holdOutStart = 0;
    holdOutEnd = 0;
    comments = '';
    tempRebStart= 0;
    tempRebEnd= 0;
    tempAdDate= 0;

    if (data[i]['ITEM CODE']) itemCode = data[i]['ITEM CODE'];
    if (data[i]['Item Code']) itemCode = data[i]['Item Code'];

    if (itemCode == 0) continue;

    if (data[i]['AD#']) adNumber = data[i]['AD#'];
    if (data[i]['Ad#']) adNumber = data[i]['Ad#'];

    if (data[i]['AD DATE']) {
      tempAdDate = convertToDate(data[i]['AD DATE']);
      if (!validateDate(tempAdDate, itemCode, parmStatus)) return;
      adYear = dateTo8Number(tempAdDate);
    }

    if (data[i]['Ad Date']) {
      tempAdDate = convertToDate(data[i]['Ad Date']);
      if (!validateDate(tempAdDate, itemCode, parmStatus)) return;
      adYear = dateTo8Number(tempAdDate);
    }

    if (data[i]['ITEM DESCRIPTION']) itemDesc = data[i]['ITEM DESCRIPTION'];
    if (data[i]['Item Description']) itemDesc = data[i]['Item Description'];

    if (data[i]['MULTI']) multi = data[i]['MULTI'];
    if (data[i]['Multiple']) multi = data[i]['Multiple'];

    if (data[i]['AD SRP']) adSrp = data[i]['AD SRP'];
    if (data[i]['Ad SRP']) adSrp = data[i]['Ad SRP'];

    if (data[i]['PWADC CURRENT COST']) curCost = data[i]['PWADC CURRENT COST'];
    if (data[i]['PWADC Current Cost']) curCost = data[i]['PWADC Current Cost'];

    if (data[i]['LUMP SUM']) {lumpSum = data[i]['LUMP SUM'];
        feature = generateFeature(lumpSum);
        }   
    if (data[i]['Lump Sum']) {lumpSum = data[i]['Lump Sum'];
        feature = generateFeature(lumpSum);
        }
    if (data[i]['CO-OP REBATE PER CASE'])
      coopRebateCase = data[i]['CO-OP REBATE PER CASE'];

    if (data[i]['Coop Rebate per Case'])
      coopRebateCase = data[i]['Coop Rebate per Case'];

    if (data[i]['RETAIL REBATE PER CASE'])
      retailRebateCase = data[i]['RETAIL REBATE PER CASE'];

    if (data[i]['Retail Rebate Per Case'])
      retailRebateCase = data[i]['Retail Rebate Per Case'];

    if (data[i]['REBATE START']) {
      tempRebStart = convertToDate(data[i]['REBATE START']);
      if (!validateDate(tempRebStart, itemCode, parmStatus)) return;
      promoStart = dateTo6Number(tempRebStart);
      //promoStart3d = tempRebStart;
      promoStart3d = new Date(tempRebStart.valueOf());
      promoStart3d.setDate(promoStart3d.getDate() + 3);
      promoStart3n = dateTo6Number(promoStart3d);
    }
    if (data[i]['Rebate Start']) {
      tempRebStart = convertToDate(data[i]['Rebate Start']);
      if (!validateDate(tempRebStart, itemCode, parmStatus)) return;
      promoStart = dateTo6Number(tempRebStart);
      //promoStart3d = tempRebStart;
      promoStart3d = new Date(tempRebStart.valueOf());
      promoStart3d.setDate(promoStart3d.getDate() + 3);
      promoStart3n = dateTo6Number(promoStart3d);
    }

    if (data[i]['REBATE END']) {
      tempRebEnd = convertToDate(data[i]['REBATE END']);
      if (!validateDate(tempRebEnd, itemCode, parmStatus)) return;
      promoEnd = dateTo6Number(tempRebEnd);
      promoEndd = tempRebEnd;
    }

    if (data[i]['Rebate End']) {
      tempRebEnd = convertToDate(data[i]['Rebate End']);
      if (!validateDate(tempRebEnd, itemCode, parmStatus)) return;
      promoEnd = dateTo6Number(tempRebEnd);
      promoEndd = tempRebEnd;
    }

    if (retailRebateCase != 0) {
      holdOutStart = dateTo6Number(tempRebStart);
      holdOutEnd = dateTo6Number(tempRebEnd);
    }

    if (data[i]['COMMENTS']) comments = data[i]['COMMENTS'];
    if (data[i]['Comment']) comments = data[i]['Comment'];

    if (comments != '' && String(comments).trim().length < 20)
      //We are looking for a number here
      comments =
        'IPRO:' +
        multi +
        '/' +
        adSrp +
        ' ' +
        promoStart3d.toLocaleDateString() +
        '-' +
        promoEndd.toLocaleDateString() +
        ' Alw:' +
        comments;

    if (comments.length > 49) comments = comments.substring(0, 49);

    if (isNotDecimal(adYear)) adYear = 0;
    if (isNotDecimal(adNumber)) adNumber = 0;
    if (isNotDecimal(multi)) multi = 0;
    if (isNotDecimal(adSrp)) adSrp = 0;
    if (isNotDecimal(curCost)) curCost = 0;
    if (isNotDecimal(lumpSum)) lumpSum = 0;
    if (isNotDecimal(coopRebateCase)) coopRebateCase = 0;
    if (isNotDecimal(retailRebateCase)) retailRebateCase = 0;
    if (isNotDecimal(promoStart)) promoStart = 0;
    if (isNotDecimal(promoStart3n)) promoStart3n = 0;
    if (isNotDecimal(promoEnd)) promoEnd = 0;
    if (isNotDecimal(holdOutStart)) holdOutStart = 0;
    if (isNotDecimal(holdOutEnd)) holdOutEnd = 0;

    if (itemCode > 99999) {
      parmStatus.statement = '90';
      parmStatus.message =
        'Item code ' + itemCode + ' in spreadsheet is too log. Max 5 digits';
      logError(parmStatus);
      return;
    }

  


    if (itemDesc.length > 23) {
      parmStatus.statement = '90';
      parmStatus.message =
        "Item Description for item code " +  itemCode +  " is too long. " +
        "Max length is 23, current length is " + itemDesc.length;
      logError(parmStatus);
      return;
    } 
    
    if (adNumber > 99) {
      parmStatus.statement = '90';
      parmStatus.message =
        "The 'Ad Number'  for item code " +
        itemCode +
        ' is too long. Max 2 digits';
      logError(parmStatus);
      return;
    }
    if (multi > 99) {
      parmStatus.statement = '90';
      parmStatus.message =
        "The 'multiple' number for item code " +
        itemCode +
        ' is too long. Max 2 digits';
      logError(parmStatus);
      return;
    }
    if (curCost > 999.99) {
      parmStatus.statement = '90';
      parmStatus.message =
        "The 'PWADC Current Cost' for item code " +
        itemCode +
        ' is too long. Max 999.99';
      logError(parmStatus);
      return;
    }
    if (lumpSum > 99999.99) {
      parmStatus.statement = '90';
      parmStatus.message =
        "The 'Lump Sum' amount for item code " +
        itemCode +
        ' is too long. Max 99999.99';
      logError(parmStatus);
      return;
    }
    if (coopRebateCase > 999.99) {
      parmStatus.statement = '90';
      parmStatus.message =
        "The 'Coop Rebate per Case' amount for item code " +
        itemCode +
        ' is too long. Max 999.99';
      logError(parmStatus);
      return;
    }
    if (retailRebateCase > 999.99) {
      parmStatus.statement = '90';
      parmStatus.message =
        "The 'Retail Rebate Per Case' amount for item code " +
        itemCode +
        ' is too long. Max 999.99';
      logError(parmStatus);
      return;
    }

    if (promoStart == 0 || promoEnd == 0) {
      parmStatus.statement = '90';
      parmStatus.message =
        'The Start or End date for item code ' +
        itemCode +
        ' is not a valid date';
      logError(parmStatus);
      return;
    }

    if (adYear == 0) {
      parmStatus.statement = '90';
      parmStatus.message =
        'The Ad date for item code ' + itemCode + ' is not a valid date';
      logError(parmStatus);
      return;
    }

    try {
      pjs.query(sqlquery, [
        adYear,
        adNumber,
        itemCode,
        itemDesc,
        multi,
        adSrp,
        curCost,
        feature,
        lumpSum,
        retailRebateCase,
        coopRebateCase,
        holdOutStart, //retbegdat  (populate only if retail REBATE PER CASE has a value)
        holdOutEnd, //retenddat  (populate only if retail REBATE PER CASE has a value)
        promoStart, //coopbegdat
        promoEnd, //coopenddat
        comments,
      ]);
      if (sqlstate >= '02000') {
        parmStatus.statement = '30';
        parmStatus.state = sqlstate;
        logError(parmStatus);
        return;
      }
    } catch (err) {
      parmStatus.statement = '91';
      parmStatus.message = 'Sql Insert error';
      logError(parmStatus);
      return;
    }
  }

  function logError(parmStatus) {
    var pgm = 'importxlxs';
    var tz = pjs.timestamp();
    var message =
      tz +
      ' ' +
      pgm +
      ':  statement=' +
      parmStatus.statement +
      ',  state=' +
      parmStatus.state;
    console.log(message);
  }
} //end of importxlsx()

exports.run = importxlsx;
exports.parms = [
  { type: 'char', length: 256 },
  { type: 'char', length: 256 },
  {
    type: 'ds',
    qualified: true,
    elements: {
      statement: { type: 'char', length: 2 },
      state: { type: 'char', length: 7 },
      message: { type: 'char', length: 100 },
    },
  },
];

function dateTo6Number(theDate) {
  try {
    var year = theDate.getFullYear() % 2000;
    var month = theDate.getMonth() + 1;
    var day = theDate.getDate();
    var number = month * 10000 + day * 100 + year;
    if (isNaN(number)) {
      return 0;
    } else {
      return number;
    }
  } catch (err) {
    return 0;
  }
}

function dateTo8Number(theDate) {
  try {
    var year = theDate.getFullYear();
    var month = theDate.getMonth() + 1;
    var day = theDate.getDate();
    var number = month * 1000000 + day * 10000 + year;
    if (isNaN(number)) {
      return 0;
    } else {
      return number;
    }
  } catch (err) {
    return 0;
  }
}

function validateDate(theDate, itemCode, parmStatus) {
  var isValidDate = true;
  var year;
  try {
    year = theDate.getFullYear();
  } catch (err) {
    isValidDate = false;
    parmStatus.statement = '40';
    parmStatus.message =
      'Date Error for item code ' + itemCode + '. Correct date and rerun.';
    console.log(parmStatus.message);
  }
  return isValidDate;
}

//function getYearFromDate(theDate)
//{
//    var year;
//    try{
//        year = theDate.getFullYear();
//    }
//    catch(err){
//        year = 0
//    }
//return year;
//}

function convertToDate(dateOrString) {
  var convertedDate;
  var splitArray;
  var dateString;
  convertedDate = dateOrString;

  try {
    var year = theDate.getFullYear();
    return convertedDate;
  } catch (err) {
    //03/25/2021  -->  2021-03-25
    splitArray = String(dateOrString).split('/');
    if (splitArray.length != 3) return convertedDate;
    dateString = splitArray[2] + '-' + splitArray[0] + '-' + splitArray[1];
    try {
      convertedDate = new Date(dateString);
      return convertedDate;
    } catch (err2) {
      return 0;
    }
  }
}

function isNotDecimal(numberIn) {
  var isDecimal = false;
  if (!isNaN(numberIn) && String(numberIn).trim().length > 0) isDecimal = true;

  return !isDecimal;
}

function generateFeature(lumpSum) {
    var feature = 0;
  try {
    if (lumpSum == 1500) {
      feature = 1;
      return feature;
    } else if (lumpSum == 1000) {
      feature = 2;
      return feature;
    } else if (lumpSum == 500) {
      feature = 3;
      return feature;
    } else {
      feature = 0;
      return feature;
    }
  } catch (err) {
    return 0;
  }
}
