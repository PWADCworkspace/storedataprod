function refreshstores() {

    var url = 'http://10.1.1.225/portal/stores/';

    var dataSource = 'PORTAL';

    try {

        var stores = pjs.sendRequest({
            method: "get",
            uri: url
        });   
        
        //pjs.query(`DELETE FROM storesp WHERE DATASOURCE = '${dataSource}' with NONE`);
        pjs.query("DELETE FROM profound.storesp WHERE DATASOURCE = ? with NONE", [dataSource]);
    
        var sqlquery =  "insert into profound.storesp " + 
        "(StoreNum, MarqueName, DataSource, UpdatedUsr, UpdatedTmz)  " +
        "values(?,?,?,?,CURRENT_TIMESTAMP) with NONE";
    
        for (var i in stores) {
    
            var store = stores[i];
    
            var id = store.store_number;
    
            try {
    
                if(id == null) {
                    console.error('Unable to process store due to null store number: ' + JSON.stringify(store))
                }
                else {
                    pjs.query(sqlquery, [
                        id,
                        (store.name == null) ? "" : store.name,
                        dataSource,
                        pjs.getUser().toUpperCase()
                        ]);
                }
            }
            catch(exception) {
                console.error(`An error occured while attempting to refresh ${dataSource} data source record for store ${id}: ${exception}`);
            }
        }
    }
    catch(error) {
        console.error(`An error occured while attempting to refresh ${dataSource} data source: ${error}`);
    }   
}

exports.refreshstores = refreshstores;