function splitState(city){
    var returnObj = {};
    returnObj.city = city
    returnObj.state = "";
    var cityLength = 0;

    var splitArray = city.split(",");
    if (splitArray.length == 2)
    {
        returnObj.city = splitArray[0].trim();
        returnObj.state = splitArray[1].trim();
        returnObj.state = returnObj.state.substring(0, 2);

    }
    if (splitArray.length == 1)
    {
        city =  city.trim();
        cityLength = city.length;
        if (cityLength >= 5)
        {
            state = city.substring(cityLength - 3).trim();
            if (state.length == 2)
            {
                returnObj.state = state;
                returnObj.city = city.substring(0, cityLength - 4).trim();
            }
        }
    }


    return returnObj;

}
exports.splitState=splitState;