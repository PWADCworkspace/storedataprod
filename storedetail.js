const fspath = require("path");
const fs       = pjs.require("InHouseApps" + fspath.sep +  "storedataprod" + fspath.sep +  "customerdata_foodshow_refreshstore.js");
const portal   = pjs.require("InHouseApps" + fspath.sep +  "storedataprod" + fspath.sep +  "customerdata_portal_refreshstore.js");
const rpms     = pjs.require("InHouseApps" + fspath.sep +  "storedataprod" + fspath.sep +  "customerdata_rpms_refreshstore.js");
const apfile   = pjs.require("InHouseApps" + fspath.sep +  "storedataprod" + fspath.sep +  "customerdata_apfile_refreshstore.js");
const arfile   = pjs.require("InHouseApps" + fspath.sep +  "storedataprod" + fspath.sep +  "customerdata_arfile_refreshstore.js");
const storefil = pjs.require("InHouseApps" + fspath.sep +  "storedataprod" + fspath.sep +  "customerdata_storefil_refreshstore.js");

// const auth     = pjs.require("InHouseApps" + fspath.sep +  "inhousemainmenu" + fspath.sep +  "authCheck.js");

async function storedetail(idIn) {
    pjs.define("idIn", {
        type: 'packed', length: 5, decimals: 0, refParm: idIn
    });

    pjs.defineDisplay("display", "InHouseApps" + fspath.sep +  "storedataprod" + fspath.sep +  "storedetail.json");


    let name = pjs.getUser();
    //name = 'thobbs';  //################## REMOVE THIS BEFORE RUNNING ON 400 ################
    let userName = name.toUpperCase();

    id = idIn;

    while (!cancel) {

        var storesp = await pjs.query("SELECT * FROM profound.storesp WHERE StoreNum = ? ORDER BY CASE WHEN DataSource = 'EX' THEN 1 ELSE 2 END, DataSource ASC", [id]);
        //console.log('store sp: ' + JSON.stringify(storesp));
        if (storesp)
        display.detail.replaceRecords(storesp);

        display.storedetail.execute();

        if(cancel){
            display.close();
        }

        if (refresh){
            await fs.refreshstore(id);
            await rpms.refreshstore(id);
            await portal.refreshstore(id);
            await apfile.refreshstore(id);
            await arfile.refreshstore(id);
            await storefil.refreshstore(id);
        }  
        if (edit) 
        {
            // if (auth.authCheck(userName, 'STOREMASTL', 'change'))  
            await doEdit(id);
            
        
        }
    }





    async function doEdit(id){   
    exit1=false;
    exit2=false;
      while(!exit1 && !exit2)
      {
        var sqlquery =  "SELECT * FROM profound.storesp " +
                        "WHERE StoreNum = ? and datasource = 'EX'"; 
        var store = await pjs.query(sqlquery,[id]);
      
        if (store && store.length == 1)
        {
            STORENUM    =store[0].storenum;	
            DATASOURCE  =store[0].datasource;
            STATUS 	    =store[0].status;	
            PIG 		=store[0].pig;		
            STOREOPEN   =store[0].storeopen; 	
            MARQUENAME  =store[0].marquename;
            POBOX       =store[0].pobox;
            STRSTREET   =store[0].strstreet; 	
            STRCITY 	=store[0].strcity;	
            STRZIP 	    =store[0].strzip;		
            STATE 	    =store[0].state;		
            SHIPSTREET  =store[0].shipstreet;
            SHIPCITY    =store[0].shipcity; 	
            SHIPZIP 	=store[0].shipzip;	
            PHONENUM    =store[0].phonenum;	
            FAXNUM 	    =store[0].faxnum;		
            OWNER 	    =store[0].owner;		
            MANAGER 	=store[0].manager;	
            STOREEMAIL  =store[0].storeemail;
            OWNEREMAIL  =store[0].owneremail;
        }
    
        
        display.edit.execute();

        if (savechanges){
            sqlquery =  "update profound.storesp " +
                        "set Status = ?, " + 
                        "Pig = ?, " + 
                        "storeOpen = ?, " +
                        "MarqueName = ?, " +
                        "POBox = ?, " +
                        "StrStreet = ?, " +
                        "StrCity = ?, " +
                        "StrZip = ?, " +
                        "State = ?, " +
                        "ShipStreet = ?, " +
                        "ShipCity = ?, " +
                        "ShipZip = ?, " +
                        "phoneNum = ?, " +
                        "FaxNum = ?, " +
                        "Owner = ?, " +
                        "Manager = ?, " +
                        "storeemail = ?, " +
                        "owneremail = ?, " + 
                        "UpdatedUsr = ?, " + 
                        "UpdatedTmz = CURRENT_TIMESTAMP " + 
                        "WHERE StoreNum = ? AND  DataSource = 'EX' WITH NONE";
                        
                        await pjs.query(sqlquery, [
                            STATUS,	
                            PIG,		
                            STOREOPEN, 	
                            MARQUENAME,
                            POBOX,
                            STRSTREET, 	
                            STRCITY,	
                            STRZIP,		
                            STATE,		
                            SHIPSTREET,
                            SHIPCITY, 	
                            SHIPZIP,	
                            PHONENUM,	
                            FAXNUM,		
                            OWNER,		
                            MANAGER,	
                            STOREEMAIL,
                            OWNEREMAIL,
                            pjs.getUser().toUpperCase(),
                            id
                        ]) ; 
            exit1=true;        
        }
      }  
        


    }



}

exports.storedetail = storedetail;

exports.parms = [
    { type: 'packed', length: 5, decimals: 0 }
]