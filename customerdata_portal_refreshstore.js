function refreshstore(storeId) {  

    //process.env["PJS_SQL_DEBUG"] = "1";

    var url = 'http://10.1.1.225/portal/stores/';

    var dataSource = 'PORTAL';

    try {

        //console.info(`Attempting to refresh ${dataSource} data source record for store ${storeId}`);
    
        var store = pjs.sendRequest({
            method: "get",
            uri: url + storeId
        });

        //delete existing datasource store entry in db
        //pjs.query(`DELETE FROM storesp WHERE DATASOURCE = '${dataSource}' AND StoreNum = '${storeId}' with NONE`);
        pjs.query("DELETE FROM storesp WHERE DATASOURCE = ? AND StoreNum = ? with NONE", [dataSource, storeId]);
    
        var sqlquery =  "insert into profound.storesp " + 
        "(StoreNum, MarqueName, DataSource, UpdatedUsr, UpdatedTmz)  " +
        "values(?,?,?,?,CURRENT_TIMESTAMP) with NONE";

        pjs.query(sqlquery, [
            storeId,
            (store.name == null) ? "" : store.name,
            dataSource,
            pjs.getUser().toUpperCase()
            ]);
    }
    catch(exception) {
        console.error(`An error occured while attempting to refresh ${dataSource} data source record for store ${storeId}: ${exception}`);
    }    
}

exports.refreshstore = refreshstore;