function refreshstore(storeId) {

        //process.env["PJS_SQL_DEBUG"] = "1";

        var url = 'http://10.1.1.225/rpms/stores/';
    
        var dataSource = 'RPMS';    

    try { 

        //console.info(`Attempting to refresh ${dataSource} data source record for store ${storeId}`);
    
        var rpmsStore = pjs.sendRequest({
            method: "get",
            uri: url + storeId
        });
    
        //delete existing datasource entry in db
        //pjs.query(`DELETE FROM storesp WHERE DATASOURCE = '${dataSource}' AND StoreNum = '${storeId}' with NONE`);
        pjs.query("DELETE FROM profound.storesp WHERE DATASOURCE = ? AND StoreNum = ? with NONE", [dataSource, storeId]);    

        var dbObject = {
            storeNumber: rpmsStore.StoreNumber,
            phoneNumber: (rpmsStore.PhoneNumber == null) ? "" : rpmsStore.PhoneNumber.toString(),
            address: (rpmsStore.StreetAddress == null) ? "" : rpmsStore.StreetAddress,
            city: (rpmsStore.City == null) ? "" : rpmsStore.City,
            state: (rpmsStore.State == null) ? "" : rpmsStore.State,
            zip: (rpmsStore.ZipCode == null) ? "" : rpmsStore.ZipCode.toString(),
            owner: (rpmsStore.Owner == null) ? "" : rpmsStore.Owner,
            manager: (rpmsStore.Manager == null) ? "" : rpmsStore.Manager,
            fax: (rpmsStore.FaxNumber == null) ? "" : rpmsStore.FaxNumber.toString(),
            source: dataSource
        };        
        
        var sqlquery = "insert into profound.storesp " + 
        "(StoreNum, phoneNum, StrStreet, StrCity, State, StrZip, Owner, Manager, FaxNum, DataSource, UpdatedUsr, UpdatedTmz)  " +
        `values('${dbObject.storeNumber}','${dbObject.phoneNumber}','${dbObject.address}','${dbObject.city}','${dbObject.state}','${dbObject.zip}','${dbObject.owner}','${dbObject.manager}','${dbObject.fax}','${dbObject.source}', '${pjs.getUser().toUpperCase()}', CURRENT_TIMESTAMP) with NONE`;

        pjs.query(sqlquery);
    }
    catch(exception) {
        console.error(`An error occured while attempting to refresh ${dataSource} data source record for store ${storeId}: ${exception}`);
    }
}

exports.refreshstore = refreshstore;

// function refreshstore(req, res) {

//     var url = 'http://localhost:3001/stores/';

//     var dataSource = 'RPMS';    

//     var storeNumber = req.params.id;    

//     var rpmsStore = pjs.sendRequest({
//         method: "get",
//         uri: url + storeNumber
//     });

//     //delete existing entry in db
//     pjs.query(`DELETE FROM profound.storesp WHERE DATASOURCE = '${dataSource}' AND StoreNum = '${storeNumber}' with NONE`);    

//     try { 

//         var dbObject = {
//             storeNumber: rpmsStore.StoreNumber,
//             phoneNumber: (rpmsStore.PhoneNumber == null) ? "" : rpmsStore.PhoneNumber.toString(),
//             address: (rpmsStore.StreetAddress == null) ? "" : rpmsStore.StreetAddress,
//             city: (rpmsStore.City == null) ? "" : rpmsStore.City,
//             state: (rpmsStore.State == null) ? "" : rpmsStore.State,
//             zip: (rpmsStore.ZipCode == null) ? "" : rpmsStore.ZipCode.toString(),
//             owner: (rpmsStore.Owner == null) ? "" : rpmsStore.Owner,
//             manager: (rpmsStore.Manager == null) ? "" : rpmsStore.Manager,
//             fax: (rpmsStore.FaxNumber == null) ? "" : rpmsStore.FaxNumber.toString(),
//             source: dataSource
//         };        
        
//         var sqlquery = "insert into profound.storesp " + 
//         "(StoreNum, phoneNum, StrStreet, StrCity, State, StrZip, Owner, Manager, FaxNum, DataSource)  " +
//         `values('${dbObject.storeNumber}','${dbObject.phoneNumber}','${dbObject.address}','${dbObject.city}','${dbObject.state}','${dbObject.zip}','${dbObject.owner}','${dbObject.manager}','${dbObject.fax}','${dbObject.source}') with NONE`;

//         pjs.query(sqlquery);
//     }
//     catch(exception) {
//         console.error(`An error occured while attempting to persist store ${storeNumber} from ${dataSource} data source: ` + exception);
//     }

//     var store = pjs.query(`SELECT * FROM profound.storesp WHERE DataSource = '${dataSource}' and StoreNum = '${storeNumber}' LIMIT 1`);

//     res.json(store);
// }

// exports.run = refreshstore;