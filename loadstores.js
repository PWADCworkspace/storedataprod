const fspath = require("path");
var fs = pjs.require("InHouseApps" + fspath.sep +  "storedataprod" + fspath.sep +  "customerdata_foodshow_refreshstores.js");
var portal = pjs.require("InHouseApps" + fspath.sep +  "storedataprod" + fspath.sep +  "customerdata_portal_refreshstores.js");
var rpms = pjs.require("InHouseApps" + fspath.sep +  "storedataprod" + fspath.sep +  "customerdata_rpms_refreshstores.js");
var arfile = pjs.require("InHouseApps" + fspath.sep +  "storedataprod" + fspath.sep +  "customerdata_arfile_refreshstores.js");
var apfile = pjs.require("InHouseApps" + fspath.sep +  "storedataprod" + fspath.sep +  "customerdata_apfile_refreshstores.js");
var storefil = pjs.require("InHouseApps" + fspath.sep +  "storedataprod" + fspath.sep +  "customerdata_storefil_refreshstores.js");
function loadStoreFromSources(){

    console.log("Starting food show data "+ pjs.timestamp());
    fs.refreshstores();
    
    console.log("Starting rpms data "+ pjs.timestamp());
    rpms.refreshstores();

    console.log("Starting portal data "+ pjs.timestamp());
    portal.refreshstores();

    console.log("Starting ARFILE data "+ pjs.timestamp());
    arfile.refreshstores();

    console.log("Starting APFILE data "+ pjs.timestamp());
    apfile.refreshstores();

    console.log("Starting STOREFIL data "+ pjs.timestamp());
    storefil.refreshstores();

    console.log("Completed");

}

exports.run=loadStoreFromSources;