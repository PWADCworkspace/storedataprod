function refreshstores() {

    var url = 'http://10.1.1.225/rpms/stores/';

    var dataSource = 'RPMS';

    try {

        var rpmsStores = pjs.sendRequest({
            method: "get",
            uri: url
        }); 
    
        //var deletequery = `DELETE FROM storesp WHERE DATASOURCE = '${dataSource}' with NONE`;
        var deletequery = "DELETE FROM profound.storesp WHERE DATASOURCE = ? with NONE";
    
        pjs.query(deletequery, [dataSource]);
    
        for (var i in rpmsStores) {
    
            var store = rpmsStores[i];
    
            var dbObject = {
                storeNumber: store.StoreNumber,
                phoneNumber: (store.PhoneNumber == null) ? "" : store.PhoneNumber.toString(),
                address: (store.StreetAddress == null) ? "" : store.StreetAddress,
                city: (store.City == null) ? "" : store.City,
                state: (store.State == null) ? "" : store.State,
                zip: (store.ZipCode == null) ? "" : store.ZipCode.toString(),
                owner: (store.Owner == null) ? "" : store.Owner,
                manager: (store.Manager == null) ? "" : store.Manager,
                fax: (store.FaxNumber == null) ? "" : store.FaxNumber.toString(),
                source: dataSource
            };
    
            try { 
                
                var sqlquery = "insert into profound.storesp " + 
                "(StoreNum, phoneNum, StrStreet, StrCity, State, StrZip, Owner, Manager, FaxNum, DataSource, UpdatedUsr, UpdatedTmz)  " +
                //`values('${dbObject.storeNumber}','${dbObject.phoneNumber}','${dbObject.address}','${dbObject.city}','${dbObject.state}','${dbObject.zip}','${dbObject.owner}','${dbObject.manager}','${dbObject.fax}','${dbObject.source}', '${pjs.getUser().toUpperCase()}', CURRENT_TIMESTAMP) with NONE`;
                "values(?,?,?,?,?,?,?,?,?,?,?, CURRENT_TIMESTAMP) with NONE";
                pjs.query(sqlquery,[
                    dbObject.storeNumber, 
                    dbObject.phoneNumber,
                    dbObject.address,
                    dbObject.city,
                    dbObject.state,
                    dbObject.zip,
                    dbObject.owner,
                    dbObject.manager,
                    dbObject.fax,
                    dbObject.source,
                    pjs.getUser().toUpperCase()
                ]);
            }
            catch(exception) {
                console.error(`An error occured while attempting to refresh ${dataSource} data source record for store ${dbObject.storeNumber}: ${exception}`);
            }
        }
    } catch (error) {
        console.error(`An error occured while attempting to refresh ${dataSource} data source: ${error}`);
    }  
}

exports.refreshstores = refreshstores;