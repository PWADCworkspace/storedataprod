// var dtl = pjs.require("storedetail.js");
const fspath = require("path");
// var auth = pjs.require("InHouseApps" + fspath.sep +  "inhousemainmenu" + fspath.sep +  "authCheck.js");
async function storelist(p1, p2){
    var dtl = pjs.require("InHouseApps" + fspath.sep +  "storedataprod" + fspath.sep +  "storedetail.js");
    //console.log( "p1=" + ((p1!=null) ? p1 : "null")   );
    //console.log( "p2=" + ((p2!=null) ? p2 : "null")  );
    pjs.define("idOut", { type: 'packed', length: 5, decimals: 0 });
    pjs.defineDisplay("display", "InHouseApps" + fspath.sep +  "storedataprod" + fspath.sep +  "storelist.json");


    let name = pjs.getUser();
    //name = 'thobbs';  //################## REMOVE THIS BEFORE RUNNING ON 400 ################
    let userName = name.toUpperCase();

    // if (!auth.authCheck(userName, 'STRMSTLST', 'run'))  return;


    var sqlquery;
    while(!exit){

        var storesp = await pjs.query("SELECT * FROM profound.storesp WHERE DataSource='EX' order by storenum");

        display.stores.replaceRecords(storesp);
        display.storelist.execute();

        display.stores.readChanged();
    
        if(!display.endOfData()){
            idOut = storenum;
            
            // if (auth.authCheck(userName, 'STRMSTLST', 'view'))      
            dtl.storedetail(idOut);
         
        }

        if(add){
            // if (auth.authCheck(userName, 'STRMSTLST', 'change'))  
           await doAdd();
            
        }
    }

    async function doAdd(){   
        var masterDataSource = 'EX';

        exit1=false;
        exit2=false;

        STORENUM    ='';	
        DATASOURCE  ='';
        STATUS 	    ='';	
        PIG 		='';		
        STOREOPEN   =''; 	
        MARQUENAME  ='';
        STRSTREET   =''; 	
        STRCITY 	='';	
        STRZIP 	    ='';		
        STATE 	    ='';		
        SHIPSTREET  ='';
        SHIPCITY    =''; 	
        SHIPZIP 	='';	
        PHONENUM    ='';	
        FAXNUM 	    ='';		
        OWNER 	    ='';		
        MANAGER 	='';	
        STOREEMAIL  ='';
        OWNEREMAIL  ='';

        while(!exit1 && !exit2)
        {         
            display.add.execute();

            if (savechanges) {
                sqlquery = "SELECT * FROM profound.storesp WHERE StoreNum = ? and DataSource = ?";

                var existingStore = await pjs.query(sqlquery, [STORENUM, masterDataSource ]);
               
                duplicateStoreNumber = existingStore != null && existingStore.length != 0;

                if(!duplicateStoreNumber) {

                    sqlquery =  "insert into profound.storesp " + 
                    "(STORENUM, DATASOURCE, STATUS, PIG, STOREOPEN, MARQUENAME, POBOX, STRSTREET, STRCITY, STRZIP, STATE, SHIPSTREET, SHIPCITY, SHIPZIP, PHONENUM, FAXNUM, OWNER, MANAGER, STOREEMAIL, OWNEREMAIL, UPDATEDUSR, UPDATEDTMZ)  " +
                    "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,CURRENT_TIMESTAMP) with NONE";  

                  
                    await pjs.query(sqlquery, [
                        STORENUM,	
                        masterDataSource,		
                        STATUS, 	
                        PIG,
                        STOREOPEN, 	
                        MARQUENAME,	
                        POBOX,
                        STRSTREET,		
                        STRCITY,
                        STRZIP,		
                        STATE,
                        SHIPSTREET, 	
                        SHIPCITY,	
                        SHIPZIP,	
                        PHONENUM,		
                        FAXNUM,		
                        OWNER,	
                        MANAGER,
                        STOREEMAIL,
                        OWNEREMAIL,
                        pjs.getUser().toUpperCase()
                    ]);      
                    return;
                }            
            }
        }    
    }    
}

// exports.storelist = storelist;
exports.run = storelist;